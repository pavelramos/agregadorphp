<?php

/* 

Nombre    : Scriptcitus v1.0
URL       : http://bitbucket.org/pavelramos/agregadorphp
Funciones : Agregar datos a BD, realizar consultas, imprimir resultados.
Créditos  : Class Mysql de http://programarenphp.wordpress.com

Ejemplo   :

  BD_host:localhost
  BD_nam:test
  BD_table:universidades
  
  Si queremos que script imprima http://dominio.com/?nombre="Universidad"
  
  Imprimirá todo lo que concuerde como "Universidad" en nombre.
  
*/


//Incluimos el fichero de la clase
include ('Mysql.class.php');

//Creamos la instancia que nos conecta
$db = Mysql::getInstance();

//Validando $_GET
if (isset($_GET['nombre'])) {

  //Declaramos $nombre del $_GET
  $nombre = $_GET['nombre'];

  //Validando $nombre
  if (empty($nombre)) {

    //Variable $nombre vacía
    echo "Variable \$nombre vacía.";
  }

  else {

    /*Consultando a Base de Datos
    BD_name   : test
    BD_table  : universidades
    */
    $sql = "SELECT * FROM universidades WHERE uni_nombre = '$nombre'";
    
    //Ejecutamos la query
    $result = $db->ejecutar($sql);
    
    //Verificando resultados de consulta
    if (mysql_num_rows($result) > 0) { //Si consulta encuentra resultados
      
      //Bucle para ir obteniendo los resultados
      while ($variables = $db->resultados($result)){
        echo $variables['uni_nombre'].'<br>';
        echo $variables['uni_region'].'<br>';
        echo $variables['uni_ciudad'].'<br>';
        echo $variables['uni_direccion'].'<br>';
        echo $variables['uni_sitioweb'];
      }
    }
    
    else {
    
      //Si la consulta no arroja resultados
      echo 'No se encontró universidad';
    }

  }
}
 
else {

  //Se imprime que no se ha especificado variable 'nombre'
  echo 'No se han especificado variables.';
}

?>