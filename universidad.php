<?php

//Funcion revisa( )
function revisa($data,$flags="abcdef") {
  if(strpos($flags,"a") == 0) {
    $data = strip_tags($data);
  }
  if(strpos($flags,"b")) {
    $data = trim($data); //trim whitespace
  }
  if(strpos($flags,"c")) {
    $data = stripslashes($data); //trim backslashes
  }
  if(strpos($flags,"d")) {
    $data = htmlspecialchars($data,ENT_NOQUOTES); //escaping XSS in PHP 5//
  }
  if(strpos($flags,"e")) {
    $data = filter_var($data,FILTER_SANITIZE_STRING); //works in php5 
  }
  if(strpos($flags,"f")) {
    $data = mysql_real_escape_string($data); // escape SQL injection
  }
  return $data;
}

//Modificando variables pasadas por URL 
foreach( $_GET as $var2 => $val2 ){ 
$_GET [ $var2 ] = str_replace ( "'" , "\"" , $_GET [ $var2 ]);
$_GET [ $var2 ] = str_replace ( "-" , " " , $_GET [ $var2 ]); 
}

//Incluimos el fichero de la clase
include ('Mysql.class.php');

//Creamos la instancia que nos conecta
$db = Mysql::getInstance();

//Comprobando $_GET
if ( count($_GET) > 1 || !isset($_GET['nombre']) || $_GET['nombre'] == NULL ) { 

  echo 'Estas en index.php';
}

else {
  
  //Mandando a revisar
  $nombre = revisa($_GET['nombre'],"acdef");
  
  /*Consultando a Base de Datos
    BD_name   : test
    BD_table  : universidades
  */
  $sql = "SELECT * FROM universidades WHERE uni_nombre = '$nombre'";
  
  //Ejecutamos la query
  $result = $db->ejecutar($sql);
  
  //Verificando resultados de consulta
  if (mysql_num_rows($result) > 0) { //Si consulta encuentra resultados
  
    //Bucle para ir obteniendo los resultados
    while ($variables = $db->resultados($result)) {
      echo $variables['uni_nombre'].'<br>';
      echo $variables['uni_region'].'<br>';
      echo $variables['uni_ciudad'].'<br>';
      echo $variables['uni_direccion'].'<br>';
      echo $variables['uni_sitioweb'];
    }
  }
      
  else {
    
    //Si la consulta no arroja resultados
    header('HTTP/1.0 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "La página que ha solicitado no se pudo encontrar.";
    exit();
  }  
  
}

?>